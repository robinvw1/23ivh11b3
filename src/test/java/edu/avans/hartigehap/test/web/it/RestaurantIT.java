package edu.avans.hartigehap.test.web.it;

import static org.junit.Assert.*;

import org.junit.Test;
import org.openqa.selenium.By;

public class RestaurantIT extends AbstractIT {
	
	@Test
	public void switchFromRestaurant() {
		login();
		
		get("/admin/kitchen?restaurant=PittigePannekoek");
		
		String restaurant = web.findElement(By.cssSelector(".navbar-brand")).getText();
		
		System.out.println(restaurant);
		
		if (!restaurant.equals("PittigePannekoek")) {
			fail("Switching between restaurants does not work.");
		}
	}
}
