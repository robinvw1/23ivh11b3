package edu.avans.hartigehap.web.controller.notification;

import java.util.LinkedList;
import java.util.List;

/**
 * Default implementation of the Observable<T>
 * @author derkgommers
 *
 * @param <T> Type of object to broadcast
 */
public abstract class AbstractObservable<T> implements Observable<T> {
	
	protected List<Observer<T>> observers;
	
	/**
	 * Default constructor, creates empty obsever list
	 */
	protected AbstractObservable () {
		this.observers = new LinkedList<>();
	}

	/**
	 * Broadcast object<T> to all observers with notify()
	 */
	@Override
	public void notify (T object) {
		List<Observer<T>> temp = new LinkedList<>(this.observers);
		for (Observer<T> observer : temp) {
			if (observer != null) {
				observer.notify(object);
			}
		}
	}

	/**
	 * Remove observer from list
	 */
	@Override
	public void removeObserver (Observer<T> observer) {
		this.observers.remove(observer);
	}

	/**
	 * Add observer to list
	 */
	@Override
	public void addObserver (Observer<T> observer) {
		this.observers.add(observer);
	}

}