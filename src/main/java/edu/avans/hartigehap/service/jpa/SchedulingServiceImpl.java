package edu.avans.hartigehap.service.jpa;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;

import edu.avans.hartigehap.domain.Scheduling;
import edu.avans.hartigehap.repository.SchedulingRepository;
import edu.avans.hartigehap.service.SchedulingService;

@Service("schedulingService")
@Repository
@Transactional
public class SchedulingServiceImpl implements SchedulingService {
	final Logger logger = LoggerFactory.getLogger(SchedulingServiceImpl.class);
	
	@Autowired
	private SchedulingRepository schedulingRepository;

	@Transactional(readOnly=true)
	public List<Scheduling> findAll() {
		return Lists.newArrayList(schedulingRepository.findAll());
	}

	@Transactional(readOnly=true)
	public Scheduling findById(Long id) {
		return schedulingRepository.findOne(id);
	}
	
	@Transactional(readOnly=true)
	public Page<Scheduling> findAllByPage(Pageable pageable) {
		return schedulingRepository.findAll(pageable);
	}
	
	public Scheduling save(Scheduling event) {
		return schedulingRepository.save(event);
	}
	
	public void delete (Long id) {
		schedulingRepository.delete(id);
	}

}
