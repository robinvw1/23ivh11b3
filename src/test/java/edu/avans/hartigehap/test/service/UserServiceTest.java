package edu.avans.hartigehap.test.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import edu.avans.hartigehap.domain.User;
import edu.avans.hartigehap.service.UserService;

public class UserServiceTest extends AbstractServiceTest {
	
	@Autowired private UserService userService;
	
	@Test
	public void create () {
		User u = createUserHelper();
		assertTrue(findContains(u));
	}
	
	private User createUserHelper () {
		User u = new User();
		u.setFirstName("Derk");
		u.setLastName("Gommers");
		u.setUsername("dgommers");
		u.setPassword(u.getLastName());
		u.setEnabled(true);
		userService.save(u);
		return u;
	}

	@Test
	public void findByName () {
		User a = createUserHelper();
		User b = userService.findByUsername(a.getUsername());
		assertEquals(a, b);
	}
	
	public boolean findContains (User u) {
		List<User> users = userService.findAll();
		assertNotNull(users);
		return users.contains(u);
	}

	@Test
	public void delete () {
		User u = createUserHelper();
		assertTrue(findContains(u));
		userService.delete(u.getId());
		assertFalse(findContains(u));
	}

	@Test
	public void update () {
		String newName = "Hendrik";
		User a = createUserHelper();
		a.setFirstName(newName);
		userService.save(a);

		User b = userService.findById(a.getId());
		assertEquals(newName, b.getFirstName());
	}
	
	@Test
	public void passwordEncryption () {
		User a = createUserHelper();
		assertTrue(!a.getLastName().equals(a.getPassword()));
	}

}
