package edu.avans.hartigehap.test;

import static org.junit.Assert.assertEquals;

import java.util.Collection;

import lombok.Getter;
import lombok.Setter;

import org.junit.Test;

import edu.avans.hartigehap.domain.Drink;
import edu.avans.hartigehap.domain.Meal;
import edu.avans.hartigehap.domain.MenuItem;
import edu.avans.hartigehap.domain.Order;
import edu.avans.hartigehap.domain.OrderItem;
import edu.avans.hartigehap.domain.orderstate.OrderState;
import edu.avans.hartigehap.domain.orderstate.OrderStateContext;

public class OrderTest extends AbstractTest {
	
	@Test
	public void nextState ()
	{
		OrderStateContext context = new MockedContext();
		context.setState(OrderState.CREATED);
		
		OrderState[] expect = {
			OrderState.SUBMITTED,
			OrderState.PLANNED, 
			OrderState.PREPARED, 
			OrderState.SERVED
		};
		for (OrderState os : expect) {
			context.getState().nextState(context);
			assertEquals(context.getState(), os);
		}
	}
	
	@Test
	public void isEmpty ()
	{
		Order order = new Order();
		assertEquals(true, order.isEmpty());
		MenuItem Pizza = new Meal();
		MenuItem Cola = new Drink();
		order.addOrderItem(Pizza);
		assertEquals(false, order.isEmpty());
		order.addOrderItem(Cola);
		assertEquals(false, order.isEmpty());
		order.deleteOrderItem(Pizza);
		assertEquals(false, order.isEmpty());
		order.deleteOrderItem(Cola);
		assertEquals(true, order.isEmpty());
	}
	
	@Test
	public void getPrice ()
	{
		Order order = new Order();
		assertEquals(0, order.getPrice());
	}
	
	@Test
	public void orderItems ()
	{
		Order order = new Order();
		assertEquals(true, order.isEmpty());
		MenuItem Pizza = new Meal();
		MenuItem Cola = new Drink();
		order.addOrderItem(Pizza);
		assertEquals(false, order.isEmpty());
		
		Collection<OrderItem> orderItems = order.getOrderItems();
		for (OrderItem orderItem : orderItems) {
			assertEquals(Pizza, orderItem.getMenuItem());
		}
		
		order.addOrderItem(Cola);
		assertEquals(false, order.isEmpty());
		order.deleteOrderItem(Pizza);
		assertEquals(false, order.isEmpty());
		
		for (OrderItem orderItem : orderItems) {
			assertEquals(Cola, orderItem.getMenuItem());
		}
		
		order.deleteOrderItem(Cola);
		assertEquals(true, order.isEmpty());
	}
	
	@Test
	public void isState ()
	{
		Order order = new Order();
		Order order2 = new Order();
		order2.nextState();
		assertEquals(true, order.isState(order.getState()));
		assertEquals(false, order.isState(order2.getState()));
	}
	
	@Getter @Setter 
	public class MockedContext implements OrderStateContext {
		private OrderState state;
	}

}
