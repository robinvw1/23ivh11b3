package edu.avans.hartigehap.web.controller;

import java.util.Collection;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import edu.avans.hartigehap.domain.DiningTable;
import edu.avans.hartigehap.domain.EmptyBillException;
import edu.avans.hartigehap.domain.Restaurant;
import edu.avans.hartigehap.domain.StateException;
import edu.avans.hartigehap.service.DiningTableService;
import edu.avans.hartigehap.service.RestaurantService;
import edu.avans.hartigehap.web.controller.notification.NotificationService;

@Controller
public class DiningTableController extends BaseController {

	// Views
	private static final String INDEX_VIEW = "hartigehap/diningtables/index";
	private static final String VIEW_VIEW = "hartigehap/diningtables/view";

	// Redirects
	private static final String ORDER_REDIRECT = "redirect:/order/";

	// Messages
	private static final String SUBMIT_ORDER_FAIL = "message_submit_order_fail";
	private static final String SUBMIT_ORDER_SUCCESS = "message_submit_order_success";
	private static final String SUBMIT_EMPTY_BILL_FAIL = "message_submit_empty_bill_fail";
	private static final String SUBMIT_BILL_FAIL = "message_submit_bill_fail";
	private static final String SUBMIT_BILL_SUCCESS = "message_submit_bill_success";

	// Models
	private static final String MODEL_MESSAGE = "message";
	private static final String MODEL_RESTAURANTS = "restaurants";
	private static final String MODEL_RESTAURANT = "restaurant";
	private static final String ERROR = "error";
	private static final String SUCCESS = "success";

	// Logger
	private static final String STATE_EXCEPTION = "stateException";
	private static final String DININGTABLE = "diningTable";
	private final Logger logger = LoggerFactory
			.getLogger(DiningTableController.class);

	@Autowired
	private MessageSource messageSource;
	@Autowired
	private RestaurantService restaurantService;
	@Autowired
	private DiningTableService diningTableService;

	@RequestMapping({ "/admin/", "/" })
	public String index() {
		return INDEX_VIEW;
	}

	@RequestMapping(value = "/order/{id}", method = RequestMethod.GET)
	public String view(@PathVariable("id") String id, Model model) {
		this.addTable(id, model);

		return VIEW_VIEW;
	}

	@RequestMapping(value = "/order/{id}/menuItems", method = RequestMethod.POST)
	public String addMenuItem(@PathVariable("id") String id,
			@RequestParam String item, Model model) {

		DiningTable table = this.addTable(id, model);
		diningTableService.addOrderItem(table, item);

		return ORDER_REDIRECT + id;
	}

	@RequestMapping(value = "/order/{diningTableId}/menuItems/{menuItemName}", method = RequestMethod.DELETE)
	public String deleteMenuItem(
			@PathVariable("diningTableId") String diningTableId,
			@PathVariable("menuItemName") String menuItemName, Model uiModel) {

		logger.info(DININGTABLE, diningTableId);
		logger.info("menuItemName = " + menuItemName);

		DiningTable diningTable = diningTableService.fetchWarmedUp(Long
				.valueOf(diningTableId));
		uiModel.addAttribute(DININGTABLE, diningTable);

		diningTableService.deleteOrderItem(diningTable, menuItemName);

		return ORDER_REDIRECT + diningTableId;
	}

	@RequestMapping(value = "/order/{diningTableId}", method = RequestMethod.PUT)
	public String receiveEvent(
			@PathVariable("diningTableId") String diningTableId,
			@RequestParam String event, RedirectAttributes redirectAttributes,
			Model uiModel, Locale locale) {

		logger.info("(receiveEvent) diningTable = " + diningTableId);

		DiningTable diningTable = warmupRestaurant(diningTableId, uiModel);

		// because of REST, the "event" parameter is part of the body. It
		// therefore cannot be used for
		// the request mapping so all events for the same resource will be
		// handled by the same
		// controller method; so we end up with an if statement

		switch (event) {
		case "submitOrder":
			NotificationService
					.createRefreshNotification(NotificationService.PATH_KITCHEN);
			return this.submitOrderEvent(diningTableId, diningTable, uiModel,
					locale, redirectAttributes);
		case "submitBill":
			NotificationService
					.createRefreshNotification(NotificationService.PATH_WAITER);
			return this.submitBillEvent(diningTableId, diningTable, uiModel,
					locale, redirectAttributes);
		default:
			logger.error("internal error: event " + event + "not recognized");

			return VIEW_VIEW;
		}
	}

	private String submitOrderEvent(String diningTableId,
			DiningTable diningTable, Model uiModel, Locale locale,
			RedirectAttributes redirectAttributes) {
		try {
			diningTableService.submitOrder(diningTable);
		} catch (StateException e) {
			logger.error(STATE_EXCEPTION, e);
			uiModel.addAttribute(MODEL_MESSAGE,
					getMessage(ERROR, SUBMIT_ORDER_FAIL, locale));

			// StateException triggers a rollback; consequently all Entities are
			// invalidated by Hibernate
			// So new warmup needed
			diningTable = warmupRestaurant(diningTableId, uiModel);

			return VIEW_VIEW;
		}

		// store the message temporarily in the session to allow displaying
		// after redirect
		redirectAttributes.addFlashAttribute(MODEL_MESSAGE,
				getMessage(SUCCESS, SUBMIT_ORDER_SUCCESS, locale));

		return ORDER_REDIRECT + diningTableId;
	}

	private String submitBillEvent(String diningTableId,
			DiningTable diningTable, Model uiModel, Locale locale,
			RedirectAttributes redirectAttributes) {
		try {
			diningTableService.submitBill(diningTable);
		} catch (EmptyBillException e) {
			logger.error("EmptyBillException", e);

			uiModel.addAttribute(MODEL_MESSAGE,
					getMessage(ERROR, SUBMIT_EMPTY_BILL_FAIL, locale));

			return VIEW_VIEW;
		} catch (StateException e) {
			logger.error(STATE_EXCEPTION, e);

			uiModel.addAttribute(MODEL_MESSAGE,
					getMessage(ERROR, SUBMIT_BILL_FAIL, locale));

			// StateException triggers a rollback; consequently all Entities are
			// invalidated by Hibernate
			// So new warmup needed
			diningTable = warmupRestaurant(diningTableId, uiModel);

			return VIEW_VIEW;
		}
		// store the message temporarily in the session to allow displaying
		// after redirect
		redirectAttributes.addFlashAttribute(MODEL_MESSAGE,
				getMessage(SUCCESS, SUBMIT_BILL_SUCCESS, locale));

		return ORDER_REDIRECT + diningTableId;
	}

	private DiningTable warmupRestaurant(String diningTableId, Model uiModel) {
		Collection<Restaurant> restaurants = restaurantService.findAll();
		uiModel.addAttribute(MODEL_RESTAURANTS, restaurants);
		DiningTable diningTable = diningTableService.fetchWarmedUp(Long
				.valueOf(diningTableId));
		uiModel.addAttribute(DININGTABLE, diningTable);
		Restaurant restaurant = restaurantService.fetchWarmedUp(diningTable
				.getRestaurant().getId());
		uiModel.addAttribute(MODEL_RESTAURANT, restaurant);

		return diningTable;
	}

	private DiningTable addTable(String diningTableId, Model model) {
		DiningTable diningTable = diningTableService.fetchWarmedUp(Long
				.valueOf(diningTableId));
		model.addAttribute(DININGTABLE, diningTable);
		return diningTable;
	}
}