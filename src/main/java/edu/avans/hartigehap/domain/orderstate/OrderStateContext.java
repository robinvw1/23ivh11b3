package edu.avans.hartigehap.domain.orderstate;

public interface OrderStateContext {
	void setState (OrderState s);
	OrderState getState ();
}