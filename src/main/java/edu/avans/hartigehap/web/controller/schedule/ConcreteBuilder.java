package edu.avans.hartigehap.web.controller.schedule;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.slf4j.Slf4j;
import edu.avans.hartigehap.domain.Scheduling;
import edu.avans.hartigehap.domain.User;

@Slf4j
public class ConcreteBuilder implements Builder {
	List<Scheduling> events;
	ServletOutputStream out;

	@Override
	public void setEvents(List<Scheduling> events) {
		this.events = events;
	}
	
	@Override
	public ServletOutputStream output(HttpServletResponse response) {
	    out = null;
	    
		try {
			out = response.getOutputStream();
			
		    Document document = new DefaultDocument();
		    
		    for (Scheduling event : events) {
		    	User user = event.getUser();
		    	
		    	out.println();
		    	out.println(user.getFirstName() + " " + user.getLastName());
		    	out.println(event.getFromDate() + "," + event.getToDate());
		    	out.println();
		    }
		    
		    document = new CorporateIdentityDecorator(document);
		    out.println(document.getOutput());
		    out.flush();
		    out.close();
		} catch (IOException e) {
			log.error("Error creating file", e);
		}
	    
	    return out;
	}

}
