package edu.avans.hartigehap.domain.orderstate;

interface OrderStateOperator {
	OrderState nextState (OrderStateContext context);
}