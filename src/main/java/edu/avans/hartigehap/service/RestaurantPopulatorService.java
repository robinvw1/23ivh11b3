package edu.avans.hartigehap.service;

public interface RestaurantPopulatorService {
	void createRestaurantsWithInventory();
}
