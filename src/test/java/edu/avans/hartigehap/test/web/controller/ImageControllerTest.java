package edu.avans.hartigehap.test.web.controller;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;

import junit.framework.Assert;

import org.junit.Test;
import org.springframework.http.MediaType;

import edu.avans.hartigehap.web.controller.image.ImageService;

public class ImageControllerTest extends AbstractControllerTest {
	
	private ImageService imageService = new ImageService();

	@Test
	public void defaultImageGeneration () throws Exception {
		this.mock.perform(get("/proxy/100x100/x/x.jpg").accept(MediaType.IMAGE_JPEG))
			.andExpect(status().isOk())
	        .andExpect(content().contentType(MediaType.IMAGE_JPEG));
	}
	
	@Test
	public void exists () {
		String defaultPath = imageService.getSourcePath("default");
		assertTrue(imageService.exists(defaultPath));
	}
	
	@Test
	public void readImage() throws Exception {

		String defaultPath = imageService.getSourcePath("default");
		byte[] defaultImage = imageService.readImage(defaultPath);
		assertNotNull(defaultImage);
		
		try {
			imageService.readImage(imageService.getSourcePath("xxxx"));
			Assert.fail("It reads a non exsitsing file!");
		} catch (IOException e) {
			// Fine!
		}
	}
	
	@Test
	public void resizeImage() throws IOException{
		String name = "default";
		int width = 180;
		int height = 200;
		
		// verify
		assertTrue("The image was resized.", imageService.resizeImage(name, width, height));
	}
	
	@Test
	public void testSaveImage () throws IOException{
		// execute
		String fileName = "default";
		String defaultPath = imageService.getSourcePath("default");
		byte[] defaultImage = imageService.readImage(defaultPath);
		
		assertTrue(imageService.saveImage(fileName, defaultImage));
	}

}