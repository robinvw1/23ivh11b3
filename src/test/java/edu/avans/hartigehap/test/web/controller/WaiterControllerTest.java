package edu.avans.hartigehap.test.web.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Test;


public class WaiterControllerTest extends AbstractControllerTest {
	
	@Test
	public void index () throws Exception {
		mock.perform(get("/admin/waiter"))
			.andExpect(status().isOk())
			.andExpect(model().attributeExists("allPreparedOrders"))
			.andExpect(model().attributeExists("allSubmittedBills"))
			.andExpect(view().name("admin/waiter/index"));
	}
	
}