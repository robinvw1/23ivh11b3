package edu.avans.hartigehap.test;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({
	"classpath:/test-root-context.xml",
	"classpath:*servlet-context.xml",
	"classpath:*web.xml"
})
public abstract class AbstractTest {

}