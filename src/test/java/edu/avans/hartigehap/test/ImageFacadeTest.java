package edu.avans.hartigehap.test;

import java.io.IOException;

import junit.framework.Assert;

import org.junit.Test;

import edu.avans.hartigehap.web.controller.image.ImageFacade;
import edu.avans.hartigehap.web.controller.image.ImageService;

public class ImageFacadeTest extends AbstractTest {

	private static final String IMAGE_NAME = "test-1";
	private ImageService service = new ImageService();
	
	@Test
	public void uploadImage () throws IOException {
		uploadImageHelper();
	}
	
	private void uploadImageHelper () throws IOException  {
		byte[] img = getImage("default");
		String[] split = IMAGE_NAME.split("-");
		Assert.assertTrue(ImageFacade.uploadImage(split[0], split[1], img));
		String end = service.getSourcePath(IMAGE_NAME);
		Assert.assertTrue("Hij bestaat wel!", service.exists(end));
	}
	
	private byte[] getImage (String name) throws IOException {
		String path = service.getSourcePath(name);
		return service.readImage(path);
	}
	
}
