package edu.avans.hartigehap.domain.orderstate;

public class SubmittedOrderState implements OrderStateOperator {

	public OrderState nextState (OrderStateContext context) {
		return OrderState.PLANNED;
	}

}