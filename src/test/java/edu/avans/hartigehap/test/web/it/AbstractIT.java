package edu.avans.hartigehap.test.web.it;

import java.util.concurrent.TimeUnit;

import lombok.extern.slf4j.Slf4j;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import edu.avans.hartigehap.test.AbstractTest;

@Slf4j
public abstract class AbstractIT extends AbstractTest {

	private static final String USER_ITEST = "itest";
	protected static WebDriver web = null;
	private static boolean shutdownHookEnabled = false;

	@Before
	public void setUpWeb() {
		if (web == null) {
			if (!shutdownHookEnabled) {
				Runtime.getRuntime().addShutdownHook(new Thread() {
					public void run() {
						close();
					}
				});
				shutdownHookEnabled = true;
			}
			/*
			 * Vervang dit eventueel door een browser naar keuze.
			 */			
			web = new FirefoxDriver();
		}

		/*
		 * Stel een timeout in die aangeeft hoe lang de webDriver moet blijven
		 * proberen om een element dat op de pagina aanwezig zou moeten zijn te
		 * selecteren.
		 */
		web.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
	}
	
	protected void login() {
		get("/admin/login");

		web.findElement(By.id("inputUser")).sendKeys(USER_ITEST);
		web.findElement(By.id("inputPassword")).sendKeys(USER_ITEST);
		web.findElement(By.className("btn-primary")).click();		
	}

	public static void close() {
		if (web == null) return;

		try {
			web.quit();
		} catch (Exception ex) {
			log.debug("", ex);
		}
		web = null;
	}

	@After
	public void tearDown() {
		close();
	}

	protected void get(String url) {
		web.get("http://localhost:8080/hh" + url);
	}
}
