package edu.avans.hartigehap.web.controller.notification;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.async.DeferredResult;

import edu.avans.hartigehap.web.controller.BaseController;

@Controller
@RequestMapping("/notifications")
public class NotificationController extends BaseController {
	
	/**
	 * Async request, waits for notification on given path
	 * @param path Refresh path
	 * @return DeferredResult
	 */
	@RequestMapping("/wait")
	@ResponseBody
	public DeferredResult<Notification> wait (@RequestParam("path") String path) {
		NotificationResult nr = new NotificationResult(path);
		return nr.getDeferredResult();
	}
	
	/**
	 * Triggers a simple refresh to given regex pages
	 * @param regex
	 * @return simple string
	 */
	@RequestMapping("/refresh")
	@ResponseBody
	public String refresh (@RequestParam("regex") String regex) { 
		NotificationService.createRefreshNotification(regex);
		return "Send";
	}

}