package edu.avans.hartigehap.web.controller.notification;

/**
 * The Generic Observer interface
 * @author derkgommers
 *
 * @param <T> Type of object for tunnel observing
 */
public interface Observer<T> {
	
	/**
	 * This method gets called by the observable to notify about
	 * the new object <T>, this method must be implemented to receive
	 * events.
	 * @param object
	 */
	void notify (T object);
}