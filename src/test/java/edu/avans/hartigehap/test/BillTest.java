package edu.avans.hartigehap.test;

import junit.framework.Assert;

import org.junit.Test;

import edu.avans.hartigehap.domain.Bill;
import edu.avans.hartigehap.domain.Drink;
import edu.avans.hartigehap.domain.MenuItem;
import edu.avans.hartigehap.domain.Order;

public class BillTest extends AbstractTest {
	
	private static final int PRICE = 10;
	private static final int COUNT = 10;

	@Test
	public void getPriceAllOrders () {
		Bill bill = new Bill();
		Assert.assertEquals(bill.getPriceAllOrders(), 0);
		
		fillOrder(bill);
		Assert.assertEquals(bill.getPriceAllOrders(), COUNT*PRICE);
	}
	
	@Test
	public void getSubmittedOrders () throws Exception {
		Bill bill = new Bill();
		fillOrder(bill);
		Assert.assertEquals(bill.getSubmittedOrders().size(), 0);
		bill.submitOrder();
		Assert.assertEquals(bill.getSubmittedOrders().size(), 1);
	}
	
	@Test
	public void getPriceSubmittedOrSuccessiveStateOrders () throws Exception {
		Bill bill = new Bill();
		fillOrder(bill);
		Assert.assertEquals(bill.getPriceSubmittedOrSuccessiveStateOrders(), 0);
		bill.submitOrder();
		fillOrder(bill);
		bill.submitOrder();
		Assert.assertEquals(bill.getPriceSubmittedOrSuccessiveStateOrders(), COUNT*PRICE*2);
	}
	
	private void fillOrder (Bill bill) {
		Order order = bill.getCurrentOrder();
		MenuItem item = new Drink("Cola", "cola", PRICE, Drink.Size.LARGE);
		for (int i = 0; i < COUNT; i++) {
			order.addOrderItem(item);
		}
	}
	
	@Test
	public void submitEmptyBill () throws Exception {
		Bill bill = new Bill();
		try {
			bill.submit();
		} catch (Exception e) {
			// Not allowed to submit empty bill
			fillOrder(bill);
			bill.submitOrder();
			bill.submit();
		}
	}
	
	@Test
	public void submitBillWithUnsubmittedOrder () throws Exception {
		Bill bill = new Bill();
		fillOrder(bill);
		try {
			bill.submit();
		} catch (Exception e) {		
			bill.submitOrder();
			bill.submit();
		}
	}

}