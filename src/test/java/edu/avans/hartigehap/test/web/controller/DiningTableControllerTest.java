package edu.avans.hartigehap.test.web.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Test;


public class DiningTableControllerTest extends AbstractControllerTest {
	
	@Test
	public void index () throws Exception {
		mock.perform(get("/"))
			.andExpect(status().isOk())
			.andExpect(view().name("hartigehap/diningtables/index"));
	}
	
	@Test
	public void detail () throws Exception {
		mock.perform(get("/order/1"))
			.andExpect(status().isOk())
			.andExpect(view().name("hartigehap/diningtables/view"))
			.andExpect(model().attributeExists("diningTable"));
	}
	
}