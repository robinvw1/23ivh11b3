<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<div xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:spring="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:joda="http://www.joda.org/joda/time/tags"
	xmlns:sec="http://www.springframework.org/security/tags" version="2.0">
	<jsp:directive.page contentType="text/html;charset=UTF-8" />
	<jsp:output omit-xml-declaration="yes" />

	<spring:message code="label_customer_info" var="labelCustomerInfo" />
	<spring:message code="label_customer_first_name"
		var="labelCustomerFirstName" />
	<spring:message code="label_customer_last_name"
		var="labelCustomerLastName" />
	<spring:message code="label_customer_birth_date"
		var="labelCustomerBirthDate" />
	<spring:message code="label_customer_description"
		var="labelCustomerDescription" />
	<spring:message code="label_customer_update" var="labelCustomerUpdate" />
	<spring:message code="label_customer_delete" var="labelCustomerDelete" />
	<spring:message code="date_format_pattern" var="dateFormatPattern" />
	<spring:message code="label_customer_photo" var="labelCustomerPhoto" />

	<spring:url value="/admin/customers/edit" var="editCustomerUrl" />
	<spring:url value="/admin/customers/delete" var="deleteCustomerUrl" />
	<spring:url value="/proxy/320x240/customer" var="customerPhotoUrl" />

	<div class="page-header">
		<h1>${labelCustomerInfo}</h1>
	</div>

	<form class="form-horizontal" role="form">
		<div class="form-group">
			<label class="col-sm-2 control-label">${labelCustomerFirstName}</label>
			<div class="col-sm-10">
				<p class="form-control-static">${customer.firstName}</p>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">${labelCustomerLastName}</label>
			<div class="col-sm-10">
				<p class="form-control-static">${customer.lastName}</p>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">${labelCustomerBirthDate}</label>
			<div class="col-sm-10">
				<p class="form-control-static">
					<joda:format value="${customer.birthDate}"
						pattern="${dateFormatPattern}" />
				</p>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">${labelCustomerDescription}</label>
			<div class="col-sm-10">
				<p class="form-control-static">${customer.description}</p>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">${labelCustomerPhoto}</label>
			<div class="col-sm-10">
				<img src="${customerPhotoUrl}/${customer.id}.jpg"
					class="form-control-static"></img>
			</div>
		</div>
		<sec:authorize access="hasRole('employee')">
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<a href="${editCustomerUrl}/${customer.id}" class="btn btn-primary">${labelCustomerUpdate}</a>&#160;
					<a href="${deleteCustomerUrl}/${customer.id}"
						class="btn btn-default">${labelCustomerDelete}</a>
				</div>
			</div>
		</sec:authorize>
	</form>
</div>