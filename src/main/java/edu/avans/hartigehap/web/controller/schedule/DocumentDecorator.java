package edu.avans.hartigehap.web.controller.schedule;

public abstract class DocumentDecorator extends Document {
	protected Document document;
	public abstract String getOutput();
	
	public DocumentDecorator(Document document) {
		this.document = document;
	}
}
