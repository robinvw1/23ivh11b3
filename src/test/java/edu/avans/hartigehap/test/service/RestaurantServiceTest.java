package edu.avans.hartigehap.test.service;

import junit.framework.Assert;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import edu.avans.hartigehap.domain.Restaurant;
import edu.avans.hartigehap.service.RestaurantService;

public class RestaurantServiceTest extends AbstractServiceTest {
	
	private static final String TEST_ID = "De Harige Knuppel";
	@Autowired private RestaurantService restaurantService;
	
	@Test
	public void create () {
		createRestaurant();
		Assert.assertTrue(exists());
	}
	
	@Test
	public void delete () {
		createRestaurant();
		restaurantService.delete(TEST_ID);
		Assert.assertFalse(exists());
	}
	
	@Test
	public void update () {
		Restaurant r = createRestaurant();
		r.setImageFileName(r.getId());
		restaurantService.save(r);
		
		Restaurant j = restaurantService.findById(TEST_ID);
		Assert.assertEquals(j.getImageFileName(), r.getId());
	}
	
	private boolean exists () {
		Restaurant f = restaurantService.findById(TEST_ID);
		return f != null;
	}
	
	private Restaurant createRestaurant () {
		Restaurant r = new Restaurant();
		r.setId(TEST_ID);
		restaurantService.save(r);
		return r;
	}
}
