package edu.avans.hartigehap.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import edu.avans.hartigehap.domain.User;

public interface UserRepository extends PagingAndSortingRepository<User, Long> {
	List<User> findByUsername(String username);
	List<User> findByEnabled(boolean enabled);
}