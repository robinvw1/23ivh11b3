package edu.avans.hartigehap.web.controller.image;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ImageService {
	
	private static final String EXTENSION = "jpg";
	private static final String SOURCE_DIRECTORY = "source";
	private static final String PATH = "proxy";
	private String basePath;
	
	
	public ImageService () {
		String path = getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
		this.basePath = path.replace("target/classes/", "")+"src/main/webapp/resources/";
	}

	/**
	 * Determines if a file at the given path exists.
	 * 
	 * @param path a file path for file
	 * @return a boolean - true if the file exists
	 */
	public boolean exists (String path) {
		File file = new File(path);
		return file.exists();
	}
	
	/**
	 * Returns the byte array of a file. The path parameter points to a file path in the resource directory.
	 * 
	 * @param path a file path of a given image file
	 * @return a byte array of an image file
	 * @throws IOException
	 */
	public byte[] readImage (String path) throws IOException {
		
        File file = new File(path);
        BufferedImage img = ImageIO.read(file);
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        ImageIO.write(img, EXTENSION, bao);
   
        return bao.toByteArray();
	}
	
	private String getRealPath (String folder, String name) {
		String s = File.separator;
		return basePath+PATH+s+folder+s+name+".jpg";
	} 
	
	public String getSourcePath (String name) {
		return this.getRealPath(SOURCE_DIRECTORY, name);
	}
	
	
	public String getPath (String name, int width, int height) {
		return this.getRealPath(width+"x"+height, name);
	}
	
	/**
	 * Creates a new directory in the resource directory. The width and height parameters are used to name the new directory.
	 * 
	 * @param width an integer to used to name the new directory
	 * @param height an integer to used to name the new directory
	 */
	public void createResolutionDirectory(int width, int height) {
		String s = File.separator;
		String path = basePath+PATH+s+width+"x"+height;
		
		if (!new File(path).exists()) {
			File  dir = new File(path);
			dir.mkdir();
			log.info("Directory " +path+ " created!");
		}
	}
	
	/**
	 * Resizes an image to a given pixel width and height.
	 * 
	 * @param name a file name for the the file to be resized
	 * @param width the new resize width in pixels
	 * @param height the new resize height in pixels
	 * @return a boolean based on the success of the resize
	 * @throws IOException
	 */
	public boolean resizeImage (String name, int width, int height) throws IOException {
		
		createResolutionDirectory(width, height);
		
		// get the original file
		String path = this.getSourcePath(name);
		File file = new File(path);
        BufferedImage originalImage = ImageIO.read(file);
        
        // resize the original image to a new image
		BufferedImage resizedImage = new BufferedImage(width, height, originalImage.getType());
		Graphics2D g = resizedImage.createGraphics();
		g.drawImage(originalImage, 0, 0, width, height, null);
		g.dispose();	
		g.setComposite(AlphaComposite.Src);
	 
		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
 
		return ImageIO.write(resizedImage, EXTENSION, new File(getPath(name, width, height)));

	}
	
	/**
	 * Saves a byte array as an image in the resource directory.
	 * <p>
	 * This method logs whether the save was successful or not.
	 * @param fileName the file name for the file that needs to be saved
	 * @param image a byte array that needs to be saved as an image
	 * @return a boolean based on the success of the file save
	 */
	public boolean saveImage (String fileName, byte[] image){
		
		String path = getSourcePath(fileName);
		File outputFile = new File(path);
		
		try (InputStream is = new ByteArrayInputStream(image)) {
		    BufferedImage buffer = ImageIO.read(is);
		    
		    try (OutputStream os = new FileOutputStream(outputFile)) {
		        ImageIO.write(buffer, EXTENSION, os);
				return true;
		    } catch (IOException e) {
		        log.error("Error while saving image", e);

		        return false;
		    }
		} catch (IOException e) {
		    log.error("Error while opening image", e);
			return false;
		}
	}
	
	/**
	 * Replaces all occurrences of a given file name in the resource directory.
	 * 
	 * @param fileName a file name that has to searched for in a directory
	 * @throws IOException if the directory that is to be searched does not exist
	 */
	public void cleanResizes (String fileName) throws IOException{
		
		List<String> resultList = new ArrayList<String>();
		List<String> subDirectoryList = new ArrayList<String>();
		
		String s = File.separator;
		File seachDirectory = new File(basePath+PATH+s);

		search(fileName, resultList, subDirectoryList, seachDirectory);
		
		// resize new images if needed
		for(String occurance: resultList){
			for(String subDir : subDirectoryList){
				if(occurance.contains(subDir)){
					
					// get the width and height values of the directory name
					String[] numbers = subDir.replaceAll("^\\D+","").split("\\D+");
					
					int width = Integer.parseInt(numbers[0]);
					int height = Integer.parseInt(numbers[1]);
					
					resizeImage(fileName, width, height);
				}
			}
		}
 
	}

	/**
	 * Searches recursively through a directory and its sub-directories for all occurrences of a file name.
	 * <p>
	 * Only searches a directory if the permission is granted.
	 * 
	 * @param fileName a file name that is searched for
	 * @param resultList a list of file paths for each occurrence 
	 * @param subDirectoryList a list of paths for each sub-directory that contains an occurrence
	 * @param searchDirectory the directory wherein the search has to be executed
	 */
	private void search (String fileName, List<String> resultList,
			List<String> subDirectoryList, File searchDirectory) {
		
		if (!searchDirectory.isDirectory()) { 
			return; 
		}
		
	  	log.debug("Searching directory ... " + searchDirectory.getAbsoluteFile());
	 
        //do you have permission to read this directory?	
	    if (searchDirectory.canRead()) {
			for (File temp : searchDirectory.listFiles()) {
			    if (temp.isDirectory() && temp.getName().equals(SOURCE_DIRECTORY)) {
			    	subDirectoryList.add(temp.getName());
			    	search(fileName, resultList, subDirectoryList, temp);
			    } else if (fileName.equalsIgnoreCase(temp.getName())) {
		    		resultList.add(temp.getAbsoluteFile().toString());
		    	}
		    }
	    } else {
	    	log.error(searchDirectory.getAbsoluteFile() + "Permission Denied");
	    }
	
	}

}