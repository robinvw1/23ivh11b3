package edu.avans.hartigehap.web.controller.image;

import java.io.*;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.avans.hartigehap.web.controller.BaseController;

@Controller
public class ImageController extends BaseController {
	
	@RequestMapping(value = "/proxy/{width}x{height}/{prefix}/{name}.jpg", method = RequestMethod.GET, produces = "image/jpeg")
	@ResponseBody
	public byte[] getImage (
		HttpServletRequest request,
		@PathVariable("prefix") String prefix, 
		@PathVariable("name") String name,
		@PathVariable("width") int width,
		@PathVariable("height") int height) throws IOException {
		Image image = new ProxyImage();
		image.loadFile(request, prefix+"-"+name, width, height);
        return image.getByteArray();
	}
}
