package edu.avans.hartigehap.domain.orderstate;

public class PreparedOrderState implements OrderStateOperator {

	public OrderState nextState (OrderStateContext context) {
		return OrderState.SERVED;
	}

}
