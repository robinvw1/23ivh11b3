package edu.avans.hartigehap.web.controller.schedule;

public abstract class Document {
	String output = "";
	
	/**
	 * Return the output of the current document.
	 * 
	 * @return output
	 */	
	public String getOutput() {
		return output;
	}
}
