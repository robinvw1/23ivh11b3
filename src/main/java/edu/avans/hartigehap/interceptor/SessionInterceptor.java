package edu.avans.hartigehap.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import edu.avans.hartigehap.domain.Restaurant;
import edu.avans.hartigehap.service.RestaurantService;

public class SessionInterceptor extends HandlerInterceptorAdapter {

	private static final String REQUEST_ATTRIBUTE_RESTAURANT = "restaurant";
	
	@Autowired
	private RestaurantService restaurantService;
	
	/**
	 * Pre handle all incomming requests
	 */
	public boolean preHandle (HttpServletRequest request, HttpServletResponse response, Object handler) {
		this.setRequestRestaurant(request);
		return true;
	}

	/**
	 * Set session restaurant
	 * @param request
	 * @param name
	 */
	private void setRequestRestaurant (HttpServletRequest request) {
		String name = request.getParameter(REQUEST_ATTRIBUTE_RESTAURANT);
		Restaurant restaurant;
		
		if (name != null) {
			restaurant = restaurantService.fetchWarmedUp(name);
			request.getSession().setAttribute(REQUEST_ATTRIBUTE_RESTAURANT, restaurant);
		} else {
			restaurant = (Restaurant) request.getSession().getAttribute(REQUEST_ATTRIBUTE_RESTAURANT);
			if (restaurant == null) {
				restaurant = restaurantService.findAll().get(0); // Default
			}
		}
		
		request.setAttribute(REQUEST_ATTRIBUTE_RESTAURANT, restaurant);
	}
	
	/**
	 * Public getter of the request restaurant
	 * @param request
	 * @return
	 */
	public static Restaurant getRestaurant (HttpServletRequest request) {
		return (Restaurant) request.getAttribute(REQUEST_ATTRIBUTE_RESTAURANT);
	}
	
}
