package edu.avans.hartigehap.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import edu.avans.hartigehap.domain.Scheduling;

public interface SchedulingRepository extends PagingAndSortingRepository<Scheduling, Long> {
	
}