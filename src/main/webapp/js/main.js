$(function() {
	$('.form_datetime').datetimepicker({
		format: 'dd-mm-yyyy hh:ii',
		linkFormat: 'yyyy-mm-dd hh:ii',
		autoclose : true,
		todayHighlight : true
	});
	
	$('.tab-content .submit').on('click', function(event) {
		event.preventDefault();
		
		$(this).parents('form').submit();
	});
});