package edu.avans.hartigehap.web.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import javax.validation.Valid;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import edu.avans.hartigehap.domain.Customer;
import edu.avans.hartigehap.domain.Restaurant;
import edu.avans.hartigehap.interceptor.SessionInterceptor;
import edu.avans.hartigehap.service.CustomerService;
import edu.avans.hartigehap.web.controller.image.ImageFacade;

@Controller
@PreAuthorize("hasRole('employee')")
public class CustomerController extends BaseController {

	// Views
	private static final String INDEX_VIEW = "admin/customers/index";
	private static final String ADD_VIEW = "admin/customers/add";
	private static final String VIEW_VIEW = "admin/customers/view";
	private static final String EDIT_VIEW = "admin/customers/edit";

	// Redirects
	private static final String EDIT_REDIRECT = "redirect:/admin/customers/view/";
	private static final String DELETE_REDIRECT = "redirect:/admin/customers";

	// Messages
	private static final String SUCCESS_MESSAGE = "success";
	private static final String CUSTOMER_SAVE_SUCCESS = "customer_save_success";

	// Models
	private static final String MODEL_MESSAGE = "message";
	private static final String MODEL_CUSTOMER = "customer";

	// Logger
	private final Logger logger = LoggerFactory
			.getLogger(CustomerController.class);

	@Autowired
	private MessageSource messageSource;
	@Autowired
	private CustomerService customerService;

	@RequestMapping(value = "/admin/customers", method = RequestMethod.GET)
	public String index(HttpServletRequest request, Model model) {
		Restaurant restaurant = SessionInterceptor.getRestaurant(request);

		logger.info("Listing customers");
		List<Customer> customers = customerService
				.findCustomersForRestaurant(restaurant);
		model.addAttribute("customers", customers);
		logger.info("No. of customers: " + customers.size());

		return INDEX_VIEW;
	}

	@RequestMapping(value = "/admin/customers/add", method = RequestMethod.GET)
	public String add(Model uiModel) {
		logger.info("Create customer form");

		Customer customer = new Customer();
		uiModel.addAttribute(MODEL_CUSTOMER, customer);

		return ADD_VIEW;
	}

	@RequestMapping(value = "/admin/customers/add", method = RequestMethod.POST)
	public String addPost(@Valid Customer customer,
			BindingResult bindingResult, Model uiModel,
			HttpServletRequest httpServletRequest,
			RedirectAttributes redirectAttributes, Locale locale,
			@RequestParam(value = "file", required = false) Part file) {

		logger.info("Creating customer: " + customer.getFirstName() + " "
				+ customer.getLastName());
		logger.info("Binding Result target: "
				+ (Customer) bindingResult.getTarget());
		logger.info("Binding Result: " + bindingResult);

		if (bindingResult.hasErrors()) {
			uiModel.addAttribute(MODEL_CUSTOMER, customer);

			return ADD_VIEW;
		}

		uiModel.asMap().clear();
		redirectAttributes.addFlashAttribute(MODEL_MESSAGE,
				getMessage(SUCCESS_MESSAGE, CUSTOMER_SAVE_SUCCESS, locale));

		// relate customer to current restaurant
		customer.setRestaurants(Arrays
				.asList(new Restaurant[] { SessionInterceptor
						.getRestaurant(httpServletRequest) }));

		// save to database
		Customer storedCustomer = customerService.save(customer);

		// upload file
		if (file.getSize() > 0) {
			processUploadFile(customer, file);
		}

		return EDIT_REDIRECT + storedCustomer.getId().toString();
	}

	private void processUploadFile(
			Customer customer, Part file) {
		// Process upload file
		if (file != null) {
			logger.info("File name: " + file.getName());
			logger.info("File size: " + file.getSize());
			logger.info("File content type: " + file.getContentType());

			byte[] fileContent = null;

			try {
				InputStream inputStream = file.getInputStream();

				if (inputStream == null) {
					logger.info("File inputstream is null");
				}

				fileContent = IOUtils.toByteArray(inputStream);

				ImageFacade.uploadImage(MODEL_CUSTOMER, String.valueOf(customer.getId()), fileContent);

			} catch (IOException ex) {
				logger.error("Error saving uploaded file", ex);
			}
		}
	}

	@RequestMapping(value = "/admin/customers/view/{id}", method = RequestMethod.GET)
	public String view(@PathVariable("id") Long id, Model uiModel) {
		logger.info("Show customer: " + id);

		Customer customer = customerService.findById(id);
		uiModel.addAttribute(MODEL_CUSTOMER, customer);

		return VIEW_VIEW;
	}

	@RequestMapping(value = "/admin/customers/edit/{id}", method = RequestMethod.GET)
	public String edit(@PathVariable("id") Long id, Model uiModel) {
		logger.info("Customer update form for customer: " + id);

		Customer customer = customerService.findById(id);
		uiModel.addAttribute(MODEL_CUSTOMER, customer);

		return EDIT_VIEW;
	}

	@RequestMapping(value = "/admin/customers/edit/{id}", method = RequestMethod.POST)
	public String editPost(@PathVariable("id") Long id,
			@Valid Customer customer, BindingResult bindingResult,
			Model uiModel, 
			RedirectAttributes redirectAttributes, Locale locale,
			@RequestParam(required = false) Part file) {

		if (bindingResult.hasErrors()) {
			uiModel.addAttribute(MODEL_CUSTOMER, customer);

			return EDIT_VIEW;
		}

		uiModel.asMap().clear();
		redirectAttributes.addFlashAttribute(MODEL_MESSAGE,
				getMessage(SUCCESS_MESSAGE, CUSTOMER_SAVE_SUCCESS, locale));

		Customer existingCustomer = customerService.findById(customer.getId());
		assert existingCustomer != null : "customer should exist";

		// update user-editable fields
		existingCustomer.updateEditableFields(customer);
		
		// save to database
		customerService.save(existingCustomer);
		
		// upload file
		if (file.getSize() > 0) {
			processUploadFile(customer, file);
		}

		return EDIT_REDIRECT + customer.getId().toString();
	}

	@RequestMapping(value = "/admin/customers/delete/{id}", method = RequestMethod.GET)
	public String delete(@PathVariable("id") Long id) {
		logger.info("Deleting customer: " + id);
		customerService.delete(id);

		return DELETE_REDIRECT;
	}
}