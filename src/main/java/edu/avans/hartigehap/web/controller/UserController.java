package edu.avans.hartigehap.web.controller;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import edu.avans.hartigehap.domain.User;
import edu.avans.hartigehap.service.UserService;

@Controller
@PreAuthorize("hasRole('manager')")
public class UserController extends BaseController {

	private static final String MODEL_ROLE_LIST = "roleList";
	// Views
	private static final String INDEX_VIEW = "admin/users/index";
	private static final String ADD_VIEW = "admin/users/add";
	private static final String VIEW_VIEW = "admin/users/view";
	private static final String EDIT_VIEW = "admin/users/edit";

	// Redirects
	private static final String EDIT_REDIRECT = "redirect:/admin/users/view/";
	private static final String DELETE_REDIRECT = "redirect:/admin/users";

	// Messages
	private static final String SUCCESS_MESSAGE = "success";
	private static final String USER_SAVE_SUCCESS = "user_save_success";

	// Models
	private static final String MODEL_MESSAGE = "message";
	private static final String MODEL_USER = "user";

	// Logger
	final Logger logger = LoggerFactory.getLogger(UserController.class);

	@Autowired
	private MessageSource messageSource;
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/admin/users", method = RequestMethod.GET)
	public String index(Model model) {
		List<User> users = userService.findAll();
		model.addAttribute("users", users);

		return INDEX_VIEW;
	}

	@RequestMapping(value = "/admin/users/add", method = RequestMethod.GET)
	public String add(Model uiModel) {
		User user = new User();
		uiModel.addAttribute(MODEL_USER, user);
		uiModel.addAttribute(MODEL_ROLE_LIST, User.getRoleOptions());
		return ADD_VIEW;
	}

	@RequestMapping(value = "/admin/users/add", method = RequestMethod.POST)
	public String addPost(@Valid User user, BindingResult bindingResult,
			Model uiModel, HttpServletRequest httpServletRequest,
			RedirectAttributes redirectAttributes, Locale locale) {
		
		uiModel.addAttribute(MODEL_ROLE_LIST, User.getRoleOptions());
		if (bindingResult.hasErrors()) {
			uiModel.addAttribute(MODEL_USER, user);

			return ADD_VIEW;
		}

		uiModel.asMap().clear();
		redirectAttributes.addFlashAttribute(MODEL_MESSAGE,
				getMessage(SUCCESS_MESSAGE, USER_SAVE_SUCCESS, locale));

		// to get the auto generated id
		User storedUser = userService.save(user);

		return EDIT_REDIRECT + storedUser.getId().toString();
	}

	@RequestMapping(value = "/admin/users/view/{id}", method = RequestMethod.GET)
	public String view(@PathVariable("id") Long id, Model uiModel) {
		User user = userService.findById(id);
		uiModel.addAttribute(MODEL_USER, user);

		return VIEW_VIEW;
	}

	@RequestMapping(value = "/admin/users/edit/{id}", method = RequestMethod.GET)
	public String edit(@PathVariable("id") Long id, Model uiModel) {
		logger.info("User update form for user: " + id);
		
		User user = userService.findById(id);
		uiModel.addAttribute(MODEL_USER, user);
		uiModel.addAttribute(MODEL_ROLE_LIST, User.getRoleOptions());
		
		return EDIT_VIEW;
	}

	@RequestMapping(value = "/admin/users/edit/{id}", method = RequestMethod.POST)
	public String editPost(@PathVariable("id") Long id, @Valid User user,
			BindingResult bindingResult, Model uiModel,
			RedirectAttributes redirectAttributes, Locale locale) {

		uiModel.addAttribute(MODEL_ROLE_LIST, User.getRoleOptions());
		if (bindingResult.hasErrors()) {
			uiModel.addAttribute(MODEL_USER, user);
			
			return EDIT_VIEW;
		}
		
		uiModel.asMap().clear();
		redirectAttributes.addFlashAttribute(
				MODEL_MESSAGE, getMessage(SUCCESS_MESSAGE, USER_SAVE_SUCCESS, locale));

		User existingUser = userService.findById(user.getId());
		assert existingUser != null : "user should exist";

		// update user-editable fields
		existingUser.update(user);
		userService.save(existingUser);

		return EDIT_REDIRECT + existingUser.getId().toString();
	}

	@RequestMapping(value = "/admin/users/delete/{id}", method = RequestMethod.GET)
	public String delete(@PathVariable("id") Long id) {
		logger.info("Deleting user: " + id);
		userService.delete(id);

		return DELETE_REDIRECT;
	}
}