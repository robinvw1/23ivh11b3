package edu.avans.hartigehap.test.web.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Test;


public class CustomerControllerTest extends AbstractControllerTest {
	
	
	@Test
	public void add () throws Exception {
		mock.perform(get("/admin/customers/add"))
			.andExpect(status().isOk())
			.andExpect(view().name("admin/customers/add"))
			.andExpect(model().attributeExists("customer"));
	}
	
	@Test
	public void detail () throws Exception {
		mock.perform(get("/admin/customers/view/1"))
			.andExpect(status().isOk())
			.andExpect(model().attributeExists("customer"))
			.andExpect(view().name("admin/customers/view"));
	}
	
	@Test
	public void edit () throws Exception {
		mock.perform(get("/admin/customers/edit/1"))
			.andExpect(status().isOk())
			.andExpect(model().attributeExists("customer"))
			.andExpect(view().name("admin/customers/edit"));
	}
	
}