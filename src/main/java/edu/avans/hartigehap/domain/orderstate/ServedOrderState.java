package edu.avans.hartigehap.domain.orderstate;

public class ServedOrderState implements OrderStateOperator {

	public OrderState nextState (OrderStateContext context) {
		return context.getState();
	}

}