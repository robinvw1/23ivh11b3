package edu.avans.hartigehap.test.web.controller;

import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import edu.avans.hartigehap.test.AbstractTest;

@WebAppConfiguration
public abstract class AbstractControllerTest extends AbstractTest {
	
	@Autowired protected WebApplicationContext wac;
	protected MockMvc mock;
	
	@Before
	public void setUpMock () {
		mock = MockMvcBuilders.webAppContextSetup(wac).dispatchOptions(true).build();
	}
	
}