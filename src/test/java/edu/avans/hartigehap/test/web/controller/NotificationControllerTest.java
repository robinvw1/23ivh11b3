package edu.avans.hartigehap.test.web.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.request.async.DeferredResult;

import edu.avans.hartigehap.web.controller.notification.Notification;
import edu.avans.hartigehap.web.controller.notification.NotificationController;

public class NotificationControllerTest extends AbstractControllerTest {
	
	@Autowired private NotificationController notificationController;
	
	@Test
	public void longPollingResult () {
		String path = "/test/543/x";
		String regex = "/test/([0-9]+)/x";
		
		DeferredResult<Notification> r = notificationController.wait(path);
		assertNull(r.getResult());
		
		notificationController.refresh(regex);
		Notification n = (Notification) r.getResult();
		assertEquals(n.getRegex(), regex);
	}
}