package edu.avans.hartigehap.service.jpa;

import java.util.List;
import java.util.ListIterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.avans.hartigehap.domain.Order;
import edu.avans.hartigehap.domain.Restaurant;
import edu.avans.hartigehap.domain.StateException;
import edu.avans.hartigehap.domain.orderstate.OrderState;
import edu.avans.hartigehap.repository.OrderRepository;
import edu.avans.hartigehap.service.OrderService;

@Service("orderService")
@Repository
@Transactional(rollbackFor = StateException.class)
public class OrderServiceImpl implements OrderService {
	
	// Logger
	private static final String SUBMITTED_ORDER = "submittedOrder";
	private static final String FOR_TABLE = "forTable";
	private static final String SUBMITTED_TIME = "submittedTime";
	private static final Logger logger = LoggerFactory.getLogger(OrderServiceImpl.class);

	@Autowired
	private OrderRepository orderRepository;
	
	@Transactional(readOnly=true)
	public Order findById(Long orderId) {
		return orderRepository.findOne(orderId);
	}
	
	
	// find all submitted orders (so for complete restaurant), ordered by submit time
	// this method serves as an example of:
	// * a named query (using entityManager)
	// * a query created using a repository method name
	// * a repository with a custom method implementation
	@Transactional(readOnly=true)
	public List<Order> findSubmittedOrdersForRestaurant(Restaurant restaurant) {
		
		// a repository with a custom method implementation
		// the custom method implementation uses a named query which is
		// invoked using an entityManager
		List<Order> submittedOrdersList = orderRepository.findSubmittedOrdersForRestaurant(restaurant);
		
		logger.info("findSubmittedOrdersForRestaurant using named query");
		ListIterator<Order>	it = submittedOrdersList.listIterator();
		while(it.hasNext()) {
			Order order = it.next();
			
			logger.info(SUBMITTED_ORDER, order.getId());
			logger.info(FOR_TABLE, order.getBill().getDiningTable().getId());
			logger.info(SUBMITTED_TIME, order.getSubmittedTime());
		}
		
		// a query created using a repository method name
		List<Order> submittedOrdersListAlternative = orderRepository.
				findByStateAndBillDiningTableRestaurant(
						OrderState.CREATED, 
						restaurant,
						new Sort(Sort.Direction.ASC, "submittedTime"));
		
		logger.info("findSubmittedOrdersForRestaurant using query created using repository method name");
		ListIterator<Order>	italt = submittedOrdersListAlternative.listIterator();
		while(italt.hasNext()) {
			Order order = italt.next();
			
			logger.info(SUBMITTED_ORDER, order.getId());
			logger.info(FOR_TABLE, order.getBill().getDiningTable().getId());
			logger.info(SUBMITTED_TIME, order.getSubmittedTime());
		}

		return submittedOrdersList;
	}
	
	@Transactional(readOnly=true)
	public List<Order> findPlannedOrdersForRestaurant(Restaurant restaurant) {
		// a query created using a repository method name
		List<Order> plannedOrdersList = orderRepository.
				findByStateAndBillDiningTableRestaurant(
						OrderState.PLANNED, 
						restaurant,
						new Sort(Sort.Direction.ASC, "plannedTime"));

		return plannedOrdersList;
	
	}	
	
	@Transactional(readOnly=true)
	public List<Order> findPreparedOrdersForRestaurant(Restaurant restaurant) {
		// a query created using a repository method name
		List<Order> preparedOrdersList = orderRepository.
				findByStateAndBillDiningTableRestaurant(
						OrderState.PREPARED, 
						restaurant,
						new Sort(Sort.Direction.ASC, "preparedTime"));

		return preparedOrdersList;	
	}

	@Transactional
	public void nextState(Order o) {
		o.nextState();
	}	
}