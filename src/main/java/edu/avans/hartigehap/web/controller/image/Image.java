package edu.avans.hartigehap.web.controller.image;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

public interface Image {
	
	/**
	 * Loads an image file from the resource directory. The width and height given determine the size of
	 * the image loaded and it's location in the resource folder.
	 * <p>
	 * Throws an IOException if default image does not exist or the folder specified by the height and width parameters
	 * 
	 * @param request the request from which this method is called
	 * @param filename unique name of the image file
	 * @param width the width in pixels of the image
	 * @param height the height in pixels of the image
	 * @throws IOException 
	 */
	void loadFile (HttpServletRequest request, String filename, int width, int height) throws IOException;
	
	/**
	 * Returns a byte array of an image file.
	 * <p>
	 * Method always returns a byte array regardless if the image exists or not.
	 * 
	 * @return a byte array of an image
	 */
	byte[] getByteArray();
}   