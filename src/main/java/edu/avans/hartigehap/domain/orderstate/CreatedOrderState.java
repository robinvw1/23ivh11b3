package edu.avans.hartigehap.domain.orderstate;


public class CreatedOrderState implements OrderStateOperator {

	public OrderState nextState (OrderStateContext context) {
		return OrderState.SUBMITTED;
	}

}