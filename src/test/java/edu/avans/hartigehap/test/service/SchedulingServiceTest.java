package edu.avans.hartigehap.test.service;

import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import edu.avans.hartigehap.domain.Scheduling;
import edu.avans.hartigehap.domain.User;
import edu.avans.hartigehap.service.SchedulingService;
import edu.avans.hartigehap.service.UserService;

public class SchedulingServiceTest extends AbstractServiceTest {
	
	@Autowired
	private SchedulingService schedulingService;
	
	@Autowired
	private UserService userService;

	@Test
	public void create () {
		Scheduling s = createScheduling();
		Assert.assertTrue(contains(s));
	}
	
	private Scheduling createScheduling () {
		Scheduling s = new Scheduling();
		s.setUser(getUser());
		s.setFromDate(new DateTime());
		s.setToDate(new DateTime().plusHours(2));
		schedulingService.save(s);
		return s;
	}
	
	private boolean contains (Scheduling s) {
		return schedulingService.findAll().contains(s);
	}
	
	private User getUser () {
		User u = new User();
		u.setFirstName("Derk");
		u.setLastName("Gommers");
		u.setUsername("dgommers");
		u.setPassword(u.getLastName());
		u.setEnabled(true);
		userService.save(u);
		return u;
	}

	@Test
	public void update () {
		Scheduling s = createScheduling();
		s.setFromDate(new DateTime().minusHours(2));
		schedulingService.save(s);
		Assert.assertEquals(s, schedulingService.findById(s.getId()));
	}

	@Test
	public void delete () {
		Scheduling s = createScheduling();
		Assert.assertTrue(contains(s));
		schedulingService.delete(s.getId());
		Assert.assertFalse(contains(s));
	}

}
