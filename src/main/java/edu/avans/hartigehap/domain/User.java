package edu.avans.hartigehap.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotEmpty;
import org.joda.time.DateTime;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "USER")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
@Getter @Setter
public class User extends DomainObject {
	private static final long serialVersionUID = 1L;
	
	public static final String ROLE_CUSTOMER = "customer";
	public static final String ROLE_EMPLOYEE = "employee";
	public static final String ROLE_MANAGER = "manager";	
	private static final String SHA1_EMPTY = "da39a3ee5e6b4b0d3255bfef95601890afd80709";

	private String username;
	
	private String password;

	@NotEmpty(message = "{validation.firstname.NotEmpty.message}")
	@Size(min = 3, max = 60, message = "{validation.firstname.Size.message}")
	public String firstName;

	@NotEmpty(message = "{validation.lastname.NotEmpty.message}")
	@Size(min = 3, max = 40, message = "{validation.lastname.Size.message}")
	public String lastName;

	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@DateTimeFormat(iso = ISO.DATE)
	private DateTime birthDate;

	private boolean enabled;
	
	@ElementCollection
	@CollectionTable(name="roles", joinColumns=@JoinColumn(name="user_id"))
	@Column(name="role")
	private List<String> roles;

	public void update (User user) {
		username = user.username;
        firstName = user.firstName;
        lastName = user.lastName;
        birthDate = user.birthDate;
        roles = user.roles;
        
        if (!user.password.equals(SHA1_EMPTY)) {
        	password = user.password;
        }
	}
 	
	public void setPassword (String password) {
		this.password = new ShaPasswordEncoder().encodePassword(password, "");
	}
	
	public static List<String> getRoleOptions () {
		List<String> l = new ArrayList<>();
		l.add(ROLE_CUSTOMER);
		l.add(ROLE_EMPLOYEE);
		l.add(ROLE_MANAGER);
		return l;
	}
}
