package edu.avans.hartigehap.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import edu.avans.hartigehap.domain.User;

public interface UserService {
	List<User> findAll();
	User findById(Long id);
	User findByUsername(String username);
	User save(User user);
	void delete(Long id);
	Page<User> findAllByPage(Pageable pageable);
}