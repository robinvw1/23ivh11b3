package edu.avans.hartigehap.web.controller;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import edu.avans.hartigehap.web.form.Message;

public class BaseController {
	
	@Autowired
	private MessageSource messageSource;	
	
	
	/**
	 * Get Message instance from locale with key and type
	 * @param type
	 * @param key
	 * @param locale
	 * @return Message instance with type and message, translated.
	 */
	public Message getMessage (String type, String key, Locale locale) {
		String message = messageSource.getMessage(key, new Object[]{}, locale);
		return new Message(type, message);
	}
}