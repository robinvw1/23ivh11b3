package edu.avans.hartigehap.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import edu.avans.hartigehap.domain.orderstate.OrderState;
import edu.avans.hartigehap.domain.orderstate.OrderStateContext;


/**
 * 
 * @author Erco
 */
@Entity
@NamedQuery(name = "Order.findSubmittedOrders", query = "SELECT o FROM Order o "
		+ "WHERE o.state = edu.avans.hartigehap.domain.orderstate.OrderState.SUBMITTED "
		+ "AND o.bill.diningTable.restaurant = :restaurant "
		+ "ORDER BY o.submittedTime")
// to prevent collision with MySql reserved keyword
@Table(name = "ORDERS")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
@Getter @Setter
@ToString(callSuper=true, includeFieldNames=true, of= {"state", "orderItems"})
public class Order extends DomainObject implements OrderStateContext {
	private static final long serialVersionUID = 1L;

	@Enumerated(EnumType.STRING)
	private OrderState state;

	@Temporal(TemporalType.TIMESTAMP)
	private Date submittedTime;

	@Temporal(TemporalType.TIMESTAMP)
	private Date plannedTime;

	@Temporal(TemporalType.TIMESTAMP)
	private Date preparedTime;

	@Temporal(TemporalType.TIMESTAMP)
	private Date servedTime;

	// unidirectional one-to-many relationship.
	@OneToMany(cascade = javax.persistence.CascadeType.ALL)
	private Collection<OrderItem> orderItems = new ArrayList<OrderItem>();

	@ManyToOne()
	private Bill bill;

	public Order () {
		state = OrderState.CREATED;
	}

	@Transient
	public boolean isState (OrderState s) {
		return state.equals(s);
	}

	// transient annotation, because methods starting with are recognized by JPA as properties
	@Transient
	public boolean isEmpty () {
		return orderItems.isEmpty();
	}

	public void addOrderItem (MenuItem menuItem) {
		Iterator<OrderItem> orderItemIterator = orderItems.iterator();
		boolean found = false;
		while (orderItemIterator.hasNext()) {
			OrderItem orderItem = orderItemIterator.next();
			if (orderItem.getMenuItem().equals(menuItem)) {
				orderItem.incrementQuantity();
				found = true;
				break;
			}
		}
		if (!found) {
			OrderItem orderItem = new OrderItem(menuItem, 1);
			orderItems.add(orderItem);
		}
	}

	public void deleteOrderItem (MenuItem menuItem) {
		Iterator<OrderItem> orderItemIterator = orderItems.iterator();
		boolean found = false;
		while (orderItemIterator.hasNext()) {
			OrderItem orderItem = orderItemIterator.next();
			if (orderItem.getMenuItem().equals(menuItem)) {
				found = true;
				if (orderItem.getQuantity() > 1) {
					orderItem.decrementQuantity();
				} else {
					// orderItem.getQuantity() == 1
					orderItemIterator.remove();
				}
				break;
			}
		}
		if (!found) {
			// do nothing
		}
	}

	@Transient
	public int getPrice() {
		int price = 0;
		Iterator<OrderItem> orderItemIterator = orderItems.iterator();
		while (orderItemIterator.hasNext()) {
			price += orderItemIterator.next().getPrice();
		}
		return price;
	}
	
	public void nextState () {
		if (state != null) {
			state.nextState(this);
		}
	}
}
