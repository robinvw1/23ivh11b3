package edu.avans.hartigehap.web.controller.notification;

/**
 * Obversable interface
 * Tunneling based notifications, not change based
 * 
 * @author derkgommers
 *
 * @param <T> Type of object that needs to be broadcasted over all observers
 */
public interface Observable<T> {
	
	/**
	 * Send out a notify to all the obsevers with object T
	 * @param object
	 */
	void notify (T object);
	
	/**
	 * Unregisters an Observer
	 * @param observer
	 */
	void removeObserver (Observer<T> observer);
	
	/**
	 * Registers an Observer for new T objects from notify
	 * @param observer
	 */
	void addObserver (Observer<T> observer);
}