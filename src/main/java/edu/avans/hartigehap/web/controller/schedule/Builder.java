package edu.avans.hartigehap.web.controller.schedule;

import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import edu.avans.hartigehap.domain.Scheduling;

public interface Builder {
	
	/**
	 * Sets the events that will return in the document.
	 * 
	 * @param events 
	 */
	public void setEvents(List<Scheduling> events);
	
	/**
	 * Request the output of the current document.
	 * 
	 * @param response to print the lines to
	 * @return an output stream for the browser
	 */
	public ServletOutputStream output(HttpServletResponse response);
}