package edu.avans.hartigehap.web.controller.notification;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * The Notification object represents a GUI Notification
 * @author derkgommers
 */
@Getter 
@Setter
@AllArgsConstructor
public class Notification {
	
	private String regex;
	private Object data;
}
