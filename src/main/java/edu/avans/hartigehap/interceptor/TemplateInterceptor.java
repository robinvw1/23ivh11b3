package edu.avans.hartigehap.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.servlet.support.RequestContext;

import edu.avans.hartigehap.service.RestaurantService;
import edu.avans.hartigehap.service.UserService;

public class TemplateInterceptor extends HandlerInterceptorAdapter {

	private static final String KEY_RESTAURANTS = "restaurants";
	private static final String KEY_RESTAURANT = "restaurant";
	private static final String KEY_USER = "user_principal";
	private static final String KEY_THEME = "theme";
	private static final String KEY_URI = "uri";

	@Autowired
	private RestaurantService restaurantService;
	@Autowired
	private UserService userService;

	/**
	 * Post handling requests and add default params
	 */
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) {
		
		if (modelAndView != null) {
			ModelMap model = modelAndView.getModelMap();
			
			model.addAttribute(KEY_RESTAURANTS, restaurantService.findAll());
			model.addAttribute(KEY_RESTAURANT, SessionInterceptor.getRestaurant(request));
			
			if (request.getUserPrincipal() != null) {
				User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
				model.addAttribute(KEY_USER, userService.findByUsername(user.getUsername()));
			}
			
			RequestContext context = new RequestContext(request);
			model.addAttribute(KEY_THEME, context.getTheme().getName());
				
			String uri = request.getRequestURI().substring(
					request.getContextPath().length());
			model.addAttribute(KEY_URI, uri);
		}
	}

}
