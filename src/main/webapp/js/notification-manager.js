function NotificationManager () 
{
	var self = this;
	setTimeout(function () {
		self.poll();
	}, 2000);
}

NotificationManager.PREFIX = '/hh/';

NotificationManager.prototype.getPath = function () {
	var path = window.location.pathname;
	path = path.substr(NotificationManager.PREFIX.length);
	path = path.replace(/\/\//, '/');
	return path;
};

NotificationManager.prototype.poll = function () {
	var self = this,
		time = new Date().getTime();
	$.get(NotificationManager.PREFIX+'notifications/wait', {
		path: this.getPath(),
		r: Math.random()
	}).done(function (data) {
		window.location.reload(true);
	}).fail(function (data) {
		if (data.readyState == 4) self.poll();
	}).always(function () {
		time = Math.abs(new Date().getTime()-time)/1000;
		console.log(time+' s');
	});
};

window.nm = new NotificationManager();