package edu.avans.hartigehap.web.controller;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import edu.avans.hartigehap.web.form.Message;

@Controller
@RequestMapping("/admin/login")
public class SecurityController extends BaseController {
	
	// Views
	private static final String LOGIN_VIEW = "admin/login";
	
	// Messages
	private static final String LOGIN_FAIL = "message_login_fail";
	
	// Models
	private static final String MODEL_MESSAGE = "message";
	
	@RequestMapping("/failed")
	public String loginFail (HttpServletRequest request, Model model, Locale locale) {
		Message message = getMessage("danger", LOGIN_FAIL, locale);
		model.addAttribute(MODEL_MESSAGE, message); 
		
		return LOGIN_VIEW;
	}
	
	@RequestMapping("")
	public String getLoginForm (HttpServletRequest request, Model model, Locale locale) {		
		return LOGIN_VIEW;
	}
	
}