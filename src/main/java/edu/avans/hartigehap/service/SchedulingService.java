package edu.avans.hartigehap.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import edu.avans.hartigehap.domain.Scheduling;

public interface SchedulingService {
	List<Scheduling> findAll();
	Scheduling findById(Long id);
	Page<Scheduling> findAllByPage(Pageable pageable);
	Scheduling save(Scheduling event);
	void delete (Long id);
}
