package edu.avans.hartigehap.web.controller.image;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import lombok.Getter;

public class RealImage implements Image {
	
	@Getter
	private byte[] byteArray;

	@Override
	public void loadFile (HttpServletRequest request, String filename, int width, int height) throws IOException {
		ImageService service = new ImageService();
		String path = service.getPath(filename, width, height);
		this.byteArray = service.readImage(path);
	}

}
