package edu.avans.hartigehap.domain.orderstate;

public class PlannedOrderState implements OrderStateOperator {

	public OrderState nextState (OrderStateContext context) {
		return OrderState.PREPARED;
	}

}
