package edu.avans.hartigehap.test.web.it;

import static org.junit.Assert.*;
import org.junit.Test;
import org.openqa.selenium.By;

public class UsersIT extends AbstractIT {

	@Test
	public void randomLoginAttempt() {
		get("/admin/login");

		web.findElement(By.id("inputUser")).sendKeys("x");
		web.findElement(By.id("inputPassword")).sendKeys("y");
		web.findElement(By.className("btn-primary")).click();

		assertNotNull(web.findElement(By.className("alert")));
	}

	@Test
	public void roleCheckForCustomer() {
		get("/admin/login");

		web.findElement(By.id("inputUser")).sendKeys("customer");
		web.findElement(By.id("inputPassword")).sendKeys("customer");
		web.findElement(By.className("btn-primary")).click();

		try {
			web.findElement(By.cssSelector("#menu > #kitchen"));
			fail("Customer can access kitchen subsystem.");
		} catch (Exception e) {
		}
	}

	@Test
	public void roleCheckForEmployee() {
		get("/admin/login");

		web.findElement(By.id("inputUser")).sendKeys("employee");
		web.findElement(By.id("inputPassword")).sendKeys("employee");
		web.findElement(By.className("btn-primary")).click();

		try {
			web.findElement(By.cssSelector("#menu > #scheduling"));
			fail("Employee can access scheduling subsystem.");
		} catch (Exception e) {
		}
	}

	@Test
	public void roleCheckForManager() {
		get("/admin/login");

		web.findElement(By.id("inputUser")).sendKeys("customer");
		web.findElement(By.id("inputPassword")).sendKeys("customer");
		web.findElement(By.className("btn-primary")).click();

		try {
			web.findElement(By.cssSelector("#menu > #kitchen"));
			fail("Manager should be able to access the kitchen subsystem.");
		} catch (Exception e) {
		}
	}

	@Test
	public void accessingKitchenSubsystem() {
		get("/admin/kitchen");

		assertNotNull(web.findElement(By.id("inputPassword")));
	}

	@Test
	public void accessingWaiterSubsystem() {
		get("/admin/waiter");

		assertNotNull(web.findElement(By.id("inputPassword")));
	}

	@Test
	public void accessingCustomersSubsystem() {
		get("/admin/customers");

		assertNotNull(web.findElement(By.id("inputPassword")));
	}

	@Test
	public void accessingSchedulingSubsystem() {
		get("/admin/scheduling");

		assertNotNull(web.findElement(By.id("inputPassword")));
	}

	@Test
	public void accessingUsersSubsystem() {
		get("/admin/users");

		assertNotNull(web.findElement(By.id("inputPassword")));
	}
}
