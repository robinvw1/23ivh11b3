package edu.avans.hartigehap.web.controller.image;

import java.io.IOException;

public class ImageFacade {
	
	private ImageFacade () {
		
	}

	/**
	 * Uploads and saves a byte array as an image in the resource directory.
	 * 
	 * @param prefix a string used for representing part of the new file path
	 * @param identifier an identifier for the file to be saved
	 * @param image a byte array representing and image
	 * @return a boolean based on the success of the upload
	 * @throws IOException
	 */
	public static boolean uploadImage(
			String prefix, 
			String identifier, 
			byte[] image) throws IOException{
		
		// Process upload file
		if (image != null) {
			ImageService service = new ImageService();
			
			String fileName = prefix +"-"+ identifier;
			if (service.saveImage( fileName, image)) {
				service.cleanResizes(fileName);
				return true;
			}
		}
		return false;
	}
}
