package edu.avans.hartigehap.test.web.it;

import static org.junit.Assert.*;

import org.junit.Test;
import org.openqa.selenium.By;

public class CustomersIT extends AbstractIT {

	@Test
	public void add() {
		login();

		get("/admin/customers/add");

		web.findElement(By.id("firstName")).sendKeys("Robin");
		web.findElement(By.id("lastName")).sendKeys("van Wijngaarden");
		web.findElement(By.id("birthDate")).sendKeys("1992-10-12");
		web.findElement(By.id("description")).sendKeys(
				"Ik ben aangemaakt met Selenium.");
		web.findElement(By.className("btn-primary")).click();

		try {
			web.findElement(By.className("alert"));
			fail("Customer could not be created.");
		} catch (Exception e) {
		}
	}

}
