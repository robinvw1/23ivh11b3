package edu.avans.hartigehap.service.jpa;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.avans.hartigehap.domain.Customer;
import edu.avans.hartigehap.domain.DiningTable;
import edu.avans.hartigehap.domain.Drink;
import edu.avans.hartigehap.domain.FoodCategory;
import edu.avans.hartigehap.domain.Meal;
import edu.avans.hartigehap.domain.Restaurant;
import edu.avans.hartigehap.domain.User;
import edu.avans.hartigehap.repository.CustomerRepository;
import edu.avans.hartigehap.repository.FoodCategoryRepository;
import edu.avans.hartigehap.repository.MenuItemRepository;
import edu.avans.hartigehap.repository.RestaurantRepository;
import edu.avans.hartigehap.repository.UserRepository;
import edu.avans.hartigehap.service.RestaurantPopulatorService;

@Service("restaurantPopulatorService")
@Repository
@Transactional
public class RestaurantPopulatorServiceImpl implements RestaurantPopulatorService {
	public static final String HARTIGEHAP_RESTAURANT_NAME = "HartigeHap";
	public static final String PITTIGEPANNEKOEK_RESTAURANT_NAME = "PittigePannekoek";
	public static final String HMMMBURGER_RESTAURANT_NAME = "HmmmBurger";
	
	final Logger logger = LoggerFactory.getLogger(RestaurantPopulatorServiceImpl.class);
	
	@Autowired
	private RestaurantRepository restaurantRepository;
	@Autowired
	private FoodCategoryRepository foodCategoryRepository;
	@Autowired
	private MenuItemRepository menuItemRepository;
	@Autowired
	private CustomerRepository customerRepository;
	@Autowired
	private UserRepository userRepository;
	
	private List<Meal> meals = new ArrayList<Meal>();
	private List<FoodCategory> foodCats = new ArrayList<FoodCategory>();
	private List<Drink> drinks = new ArrayList<Drink>();
	private List<Customer> customers = new ArrayList<Customer>();

		
	/**
	 *  menu items, food categories and customers are common to all restaurants and should be created only once.
	 *  Although we can safely assume that the are related to at least one restaurant and therefore are saved via
	 *  the restaurant, we save them explicitly anyway
	 */
	private void createCommonEntities() {
		
		createFoodCategory("low fat");
		createFoodCategory("high energy");
		createFoodCategory("vegatarian");
		createFoodCategory("italian");
		createFoodCategory("asian");
		createFoodCategory("alcoholic drinks");
		createFoodCategory("energizing drinks");
		
		createMeal("spaghetti", "spaghetti.jpg", 8, "easy",
			getFoodCategories());
		createMeal("macaroni", "macaroni.jpg", 8, "easy",
			getFoodCategories());		
		createMeal("canneloni", "canneloni.jpg", 9, "easy",
			getFoodCategories());
		createMeal("pizza", "pizza.jpg", 9, "easy",
			getFoodCategories());
		createMeal("carpaccio", "carpaccio.jpg", 7, "easy",
			Arrays.<FoodCategory>asList(new FoodCategory[]{foodCats.get(3), foodCats.get(0)}));
		createMeal("ravioli", "ravioli.jpg", 8, "easy",
			Arrays.<FoodCategory>asList(new FoodCategory[]{foodCats.get(3), foodCats.get(1), foodCats.get(2)}));

		createDrink("beer", "beer.jpg", 1, Drink.Size.LARGE,
			Arrays.<FoodCategory>asList(new FoodCategory[]{foodCats.get(5)}));
		createDrink("coffee", "coffee.jpg", 1, Drink.Size.MEDIUM,
			Arrays.<FoodCategory>asList(new FoodCategory[]{foodCats.get(6)}));
		
		byte[] photo = new byte[]{127,-128,0};
		String customerSurname = "bakker";
		String customerName = "piet";
		String description = "description";
		for (int i = 0; i < 3; i++) {
			createCustomer(customerName, customerSurname, new DateTime(), 1, description, photo);
		}
	}

	private List<FoodCategory> getFoodCategories() {
		return Arrays.<FoodCategory>asList(new FoodCategory[]{foodCats.get(3), foodCats.get(1)});
	}

	private void createFoodCategory(String tag) {
		FoodCategory foodCategory = new FoodCategory(tag);
		foodCategory = foodCategoryRepository.save(foodCategory);
		foodCats.add(foodCategory);
	}
	
	private void createMeal(String name, String image, int price, String recipe, List<FoodCategory> foodCats) {
		Meal meal = new Meal(name, image, price, recipe);
		meal = menuItemRepository.save(meal);
		// as there is no cascading between FoodCategory and MenuItem (both ways), it is important to first 
		// save foodCategory and menuItem before relating them to each other, otherwise you get errors
		// like "object references an unsaved transient instance - save the transient instance before flushing:"
		meal.addFoodCategories(foodCats);
		meals.add(meal);
	}
	
	private void createDrink(String name, String image, int price, Drink.Size size, List<FoodCategory> foodCats) {
		Drink drink = new Drink(name, image, price, size);
		drink = menuItemRepository.save(drink);
		drink.addFoodCategories(foodCats);
		drinks.add(drink);
	}
	
	private void createCustomer(String firstName, String lastName, DateTime birthDate,
		int partySize, String description, byte[] photo) {
		Customer customer = new Customer(firstName, lastName, birthDate, partySize, description, photo); 
		customers.add(customer);
		customerRepository.save(customer);
	}
	
	private void createDiningTables(int numberOfTables, Restaurant restaurant) {
		for(int i=0; i<numberOfTables; i++) {
			DiningTable diningTable = new DiningTable(i+1);
			diningTable.setRestaurant(restaurant);
			restaurant.getDiningTables().add(diningTable);
		}
	}
	
	private void populateRestaurant(Restaurant restaurant) {
		
		// every restaurant has its own dining tables
		createDiningTables(5, restaurant);

		// for the moment every restaurant has all available food categories 
		for(FoodCategory foodCat : foodCats) {
			restaurant.getMenu().getFoodCategories().add(foodCat);
		}

		// for the moment every restaurant has the same menu 
		for(Meal meal : meals) {
			restaurant.getMenu().getMeals().add(meal);
		}

		// for the moment every restaurant has the same menu 
		for(Drink drink : drinks) {
			restaurant.getMenu().getDrinks().add(drink);
		}
		
		// for the moment, every customer has dined in every restaurant
		for(Customer customer : customers) {
			customer.getRestaurants().add(restaurant);
			restaurant.getCustomers().add(customer);
		}
		
		// should save everything that is reachable by cascading
		restaurantRepository.save(restaurant);
	}
	
	private void createUser (String name, String surname, String password, String username, String role) {
		List<String> roles = new LinkedList<String>();
		if (role == null) {
			roles.add(User.ROLE_CUSTOMER);
			roles.add(User.ROLE_EMPLOYEE);
			roles.add(User.ROLE_MANAGER);
		} else {
			roles.add(role);
		}
		
		User u = new User();
		u.setFirstName(name);
		u.setLastName(surname);
		u.setPassword(password);
		u.setUsername(username);
		u.setRoles(roles);
		u.setEnabled(true);
		userRepository.save(u);
	}
	
	private void createUsers () {
		String[] un = {
			"derk", "trevorr", "bram", "robin", "erco", "bob", "itest"
		};		
		
		createUser("Derk", "Gommers", un[0], un[0], null);
		createUser("Trevorr", "Marshall", un[1], un[1], User.ROLE_MANAGER);
		createUser("Bram", "van der Sanden", un[2], un[2], User.ROLE_MANAGER);
		createUser("Robin", "van Wijngaarden", un[3], un[3], User.ROLE_MANAGER);
		createUser("Erco", "Argante", un[4], un[4], User.ROLE_EMPLOYEE);
		createUser("Bob", "de Besteller", un[5], un[5], User.ROLE_CUSTOMER);
		createUser("Intergratie", "Tester", un[6], un[6], null);
	}
	
	public void createRestaurantsWithInventory() {
		
		createCommonEntities();
		createUsers();

		Restaurant restaurant = new Restaurant(HARTIGEHAP_RESTAURANT_NAME, "deHartigeHap.jpg");
		populateRestaurant(restaurant);
		
		restaurant = new Restaurant(PITTIGEPANNEKOEK_RESTAURANT_NAME, "dePittigePannekoek.jpg");
		populateRestaurant(restaurant);
		
		restaurant = new Restaurant(HMMMBURGER_RESTAURANT_NAME, "deHmmmBurger.jpg");
		populateRestaurant(restaurant);
	}	
}
