package edu.avans.hartigehap.test.web.it;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;

public class OrderIT extends AbstractIT {
	
	@Test
	public void addItem () {
		get("/order/1");
		web.findElements(By.className("btn-success")).get(0).click();
		String c = web.findElement(By.tagName("table")).findElement(By.tagName("td")).getText();
		Assert.assertEquals(c, "carpaccio");
	}
	
	@Test
	public void submitOrder () {
		addItem();
		web.findElements(By.className("btn-primary")).get(0).click();
	}
	
	@Test 
	public void planOrder () {
		submitOrder();
		login();
		get("/admin/kitchen");
		web.findElements(By.className("btn-default")).get(0).click();
	}
	
	@Test 
	public void prepareOrder () {
		planOrder();
		planOrder();
	}

}