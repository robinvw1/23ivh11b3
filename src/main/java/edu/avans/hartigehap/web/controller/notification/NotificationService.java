package edu.avans.hartigehap.web.controller.notification;

import lombok.extern.slf4j.Slf4j;

/**
 * The Observable Service to tunnel all the notifications
 * @author derkgommers
 */
@Slf4j
public class NotificationService extends AbstractObservable<Notification> {
	
	// Default notifications paths
	public static final String PATH_KITCHEN = "admin/kitchen";
	public static final String PATH_WAITER = "admin/waiter";
	public static final String PATH_TABLE = "order/([0-9]+)";

	private static NotificationService instance = new NotificationService();
	
	/**
	 * Public getter for the singleton 
	 * @return Singleton instance of NotificationService
	 */
	public static NotificationService getInstance () {
		return instance;
	}
	
	/**
	 * Static helper method to easily create refresh notifications
	 * @param regex
	 */
	public static void createRefreshNotification (String regex) {
		NotificationService ns = NotificationService.getInstance();
		log.info("Fired for path "+regex);
		ns.notify(new Notification(regex, 1));
	}
	
}