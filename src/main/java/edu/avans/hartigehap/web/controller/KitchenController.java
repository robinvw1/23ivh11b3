package edu.avans.hartigehap.web.controller;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import edu.avans.hartigehap.domain.Order;
import edu.avans.hartigehap.domain.Restaurant;
import edu.avans.hartigehap.interceptor.SessionInterceptor;
import edu.avans.hartigehap.service.OrderService;
import edu.avans.hartigehap.web.controller.notification.NotificationService;

@Controller
@PreAuthorize("hasRole('employee')")
public class KitchenController extends BaseController {

	// Views
	private static final String INDEX_VIEW = "admin/kitchen/index";

	// Models
	private static final String MODEL_KITCHEN = "kitchen";
	private static final String MODEL_WAITER = "waiter";

	@Autowired
	private MessageSource messageSource;
	@Autowired
	private OrderService orderService;

	@RequestMapping(value = "/admin/kitchen", method = RequestMethod.GET)
	public String index(HttpServletRequest request, Model model) {
		Restaurant restaurant = SessionInterceptor.getRestaurant(request);

		List<Order> allSubmittedOrders = orderService
				.findSubmittedOrdersForRestaurant(restaurant);
		model.addAttribute("allSubmittedOrders", allSubmittedOrders);

		List<Order> allPlannedOrders = orderService
				.findPlannedOrdersForRestaurant(restaurant);
		model.addAttribute("allPlannedOrders", allPlannedOrders);

		return INDEX_VIEW;
	}

	// this method serves kitchen subsystem and waiter subsystem requests,
	// which is quite confusing!
	// Reason is that the actual resource "orders/{orderId}" that is asked for,
	// is the same for kitchen subsystem and waiter subsystem,
	// meaning that the request URI is the same (according to REST).
	// You cannot have two methods with the same request URI mapping.
	// It is because the "event" request parameter (which is the distinguishing
	// parameter) is part of the HTTP body and can therefore not be
	// used for the request mapping.
	@RequestMapping(value = "/admin/orders/{orderId}", method = RequestMethod.PUT)
	public String receiveEvent(@PathVariable("orderId") String orderId,
			@RequestParam String event, Model uiModel, Locale locale) {

		Order order = orderService.findById(Long.valueOf(orderId));

		switch (event) {
		case "planOrder":
			return changeOrderState(order, NotificationService.PATH_KITCHEN,
					MODEL_KITCHEN);
		case "orderHasBeenPrepared":
			return changeOrderState(order, NotificationService.PATH_WAITER,
					MODEL_KITCHEN);
		case "orderHasBeenServed":
			return changeOrderState(order, NotificationService.PATH_TABLE,
					MODEL_WAITER);
		default:
			return this.redirect(null);
		}
	}

	private String changeOrderState(Order o, String path, String postfix) {
		NotificationService.createRefreshNotification(path);
		orderService.nextState(o);

		return redirect(postfix);
	}

	private String redirect(String postfix) {
		postfix = (postfix != null ? "/" + postfix : "");

		return "redirect:/admin/" + postfix;
	}
}