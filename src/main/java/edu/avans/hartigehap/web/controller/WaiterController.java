package edu.avans.hartigehap.web.controller;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import edu.avans.hartigehap.domain.*;
import edu.avans.hartigehap.interceptor.SessionInterceptor;
import edu.avans.hartigehap.service.*;

@Controller
@PreAuthorize("hasRole('employee')")
public class WaiterController extends BaseController {
	
	// Views
	private static final String INDEX_VIEW = "admin/waiter/index";
	

	
	// Logger
	private final Logger logger = LoggerFactory.getLogger(WaiterController.class);

	@Autowired
	private MessageSource messageSource;
	@Autowired
	private RestaurantService restaurantService;
	@Autowired
	private BillService billService;
	@Autowired
	private OrderService orderService;

	@RequestMapping(value = "/admin/waiter", method = RequestMethod.GET)
	public String index(HttpServletRequest request, Model uiModel) {
		Restaurant restaurant = SessionInterceptor.getRestaurant(request);
		
		List<Order> allPreparedOrders = 
				orderService.findPreparedOrdersForRestaurant(restaurant);
		uiModel.addAttribute("allPreparedOrders", allPreparedOrders);

		List<Bill> allSubmittedBills = 
				billService.findSubmittedBillsForRestaurant(restaurant);
		uiModel.addAttribute("allSubmittedBills", allSubmittedBills);
		
		return INDEX_VIEW;
	}
		
	@RequestMapping(value = "/bills/{billId}", method = RequestMethod.PUT)
	public String receiveBillEvent(
			@PathVariable("billId") String billId, 
			@RequestParam String event, 
			Model uiModel, Locale locale) {

		Bill bill = billService.findBillById(Long.valueOf(billId));
		
		if ("billHasBeenPaid".equals(event)) {
			String view = waiterBillHasBeenPaid(billId, uiModel, bill);
			if (view != null) {
				return view;
			}
		} else {
			logger.error("Internal error: event " + event + " not recognized");
		}
		
		return "redirect:/admin/waiter";
	}


	/**
	 * @param billId
	 * @param uiModel
	 * @param bill
	 * @return
	 */
	private String waiterBillHasBeenPaid(String billId, Model uiModel, Bill bill) {
		try {
			billService.billHasBeenPaid(bill);
		} catch (StateException e) {
			logger.error("Internal error has occurred! Order " + Long.valueOf(billId) 
					+ "has not been changed to served state!", e);
			// StateException triggers a rollback; consequently all Entities are invalidated by Hibernate
			// So new warmup needed.
			
			return INDEX_VIEW;
		}
		
		return null;
	}
}
