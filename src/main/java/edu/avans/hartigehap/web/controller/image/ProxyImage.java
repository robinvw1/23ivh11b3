package edu.avans.hartigehap.web.controller.image;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ProxyImage implements Image {
	
	private static final String DEFAULT_IMAGE_NAME = "default";
	private static final String RESIZE_IMAGE = "The requested image does not exist yet. It's creation will be attempted.";
	private static final String DEFAULT_IMAGE_RESIZED = "There is no default image of the given resolution. The default image shall be resized.";
	private static final String DEFAULT_IMAGE_DOES_NOT_EXIST = "The default image does not exist.";

	private Image realImage;

	@Override
	public void loadFile(
			HttpServletRequest request,
			String filename,
			int width,
			int height) throws IOException {
	
		this.realImage = new RealImage();
		try {
			this.realImage.loadFile(request, filename, width, height);
		} catch (IOException e) {
			
			log.info(RESIZE_IMAGE, e);
			ImageService service = new ImageService();
			String sourcePath = service.getSourcePath(filename);
			service.createResolutionDirectory(width, height);
			
			if (service.exists(sourcePath)) {
				service.resizeImage(filename, width, height);
				this.realImage.loadFile(request, filename, width, height);
			} else {
				try {
					this.realImage.loadFile(request, DEFAULT_IMAGE_NAME, width, height);
				} catch (IOException de) {
					
					log.info(DEFAULT_IMAGE_RESIZED, de);
					sourcePath = service.getSourcePath(DEFAULT_IMAGE_NAME);
					if (service.exists(sourcePath)) {
						service.resizeImage(DEFAULT_IMAGE_NAME, width, height);
						this.realImage.loadFile(request, DEFAULT_IMAGE_NAME, width, height);
					} else {
						log.error(sourcePath);
						throw new IOException(DEFAULT_IMAGE_DOES_NOT_EXIST);
					}
				}
			}
		}
	}

	@Override
	public byte[] getByteArray() {
		return this.realImage.getByteArray();
	}
}
