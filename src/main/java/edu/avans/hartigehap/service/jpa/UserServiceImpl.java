package edu.avans.hartigehap.service.jpa;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;

import edu.avans.hartigehap.domain.User;
import edu.avans.hartigehap.repository.UserRepository;
import edu.avans.hartigehap.service.UserService;

@Service("userService")
@Repository
@Transactional
public class UserServiceImpl implements UserService {
	final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
	
	@Autowired
	private UserRepository userRepository;
	

	@Transactional(readOnly=true)
	public List<User> findAll() {
		return Lists.newArrayList(userRepository.findAll());
	}

	@Transactional(readOnly=true)
	public User findById(Long id) {
		return userRepository.findOne(id);
	}
	
	@Transactional(readOnly=true)
	public User findByUsername(String username) {
		User user = null;
		
		List<User> users = userRepository.findByUsername(username);
		if (!users.isEmpty()) {
			user = users.get(0);
		}
		
		return user;
	}
	
	@Transactional(readOnly=true)
	public Page<User> findAllByPage(Pageable pageable) {
		return userRepository.findAll(pageable);
	}

	public User save(User user) {
		return userRepository.save(user);
	}

	public void delete(Long id) {
		userRepository.delete(id);
	}
}
