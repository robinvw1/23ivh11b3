package edu.avans.hartigehap.test.web.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.Test;

public class KitchenControllerTest extends AbstractControllerTest {

	@Test
	public void index () throws Exception {
		mock.perform(get("/admin/kitchen"))
			.andExpect(status().isOk())
			.andExpect(view().name("admin/kitchen/index"))
			.andExpect(model().attributeExists("allSubmittedOrders"))
			.andExpect(model().attributeExists("allPlannedOrders"));
	}
	
}