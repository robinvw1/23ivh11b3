package edu.avans.hartigehap.domain.orderstate;


public enum OrderState implements OrderStateOperator {
	
	CREATED(new CreatedOrderState()),
	SUBMITTED(new SubmittedOrderState()),
	PLANNED(new PlannedOrderState()),
	PREPARED(new PreparedOrderState()),
	SERVED(new ServedOrderState());
	
	private OrderStateOperator operator;
	
	private OrderState (OrderStateOperator operator) {
		this.operator = operator;
	}

	public OrderState nextState (OrderStateContext context) {
		OrderState state = this.operator.nextState(context);
		context.setState(state);
		return state;
	}
	
	public String toString () {
		return this.operator.getClass().getCanonicalName();
	}
}