package edu.avans.hartigehap.web.controller.schedule;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import edu.avans.hartigehap.domain.Scheduling;
import edu.avans.hartigehap.domain.User;
import edu.avans.hartigehap.service.SchedulingService;
import edu.avans.hartigehap.service.UserService;
import edu.avans.hartigehap.web.controller.BaseController;

@Controller
@PreAuthorize("hasRole('manager')")
@Slf4j
public class SchedulingController extends BaseController {
	
	// Views
	private static final String ADD_VIEW = "admin/scheduling/add";
	private static final String EXPORT_VIEW = "admin/scheduling/export";
	
	// Redirects
	private static final String INDEX_REDIRECT = "redirect:/admin/scheduling/add";
	
	// Models
	private static final String MODEL_MESSAGE = "message";
	private static final String MODEL_EVENT = "event";
	private static final String MODEL_USERS = "users";
	

	@Autowired
	private MessageSource messageSource;
	@Autowired
	private SchedulingService schedulingService;
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/admin/scheduling", method = RequestMethod.GET)
	public String index() {
		return INDEX_REDIRECT;
	}

	@RequestMapping(value = "/admin/scheduling/add", method = RequestMethod.GET)
	public String add(Model uiModel) {
		List<User> users = userService.findAll();

		uiModel.addAttribute(MODEL_USERS, users);

		return ADD_VIEW;
	}

	@RequestMapping(value = "/admin/scheduling/add", method = RequestMethod.POST)
	public String addPost(@Valid Scheduling event, BindingResult bindingResult,
			Model uiModel, HttpServletRequest httpServletRequest,
			RedirectAttributes redirectAttributes, Locale locale) {
		log.info("Creating event: " + event.getFromDate() + " "
				+ event.getToDate());
		log.info("Binding Result target: "
				+ (Scheduling) bindingResult.getTarget());
		log.info("Binding Result: " + bindingResult);

		if (bindingResult.hasErrors()) {
			List<User> users = userService.findAll();

			uiModel.addAttribute(MODEL_USERS, users);
			uiModel.addAttribute(MODEL_EVENT, event);

			return ADD_VIEW;
		}

		uiModel.asMap().clear();
		uiModel.addAttribute(MODEL_MESSAGE,
				getMessage("success", "scheduling_add_success", locale));
		
		long employee = Long.parseLong(httpServletRequest.getParameter("employee"));
		log.info("Long: "+employee);
		
		User user = userService.findById(employee);
		event.setUser(user);
		
		schedulingService.save(event);

		return ADD_VIEW;
	}
	
	@RequestMapping(value = "/admin/scheduling/export", method = RequestMethod.GET)
	public String export(HttpServletResponse response) {
		return EXPORT_VIEW;
	}

	@RequestMapping(value = "/admin/scheduling/export", method = RequestMethod.POST)
	public void exportPost(HttpServletResponse response) {
		List<Scheduling> events = schedulingService.findAll();
		log.info(events.toString());

		response.setContentType("text/csv");
		response.setHeader("Content-Disposition",
				"attachment;filename=export.csv");

		Builder builder = new ConcreteBuilder();
		builder.setEvents(events);
		builder.output(response);
	}
}