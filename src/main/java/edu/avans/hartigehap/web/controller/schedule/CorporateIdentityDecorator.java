package edu.avans.hartigehap.web.controller.schedule;

public class CorporateIdentityDecorator extends DocumentDecorator {
	private static final String divider = "===========================";
	
	public CorporateIdentityDecorator(Document document) {
		super(document);
	}
	
	public String getOutput() {
		String output = document.getOutput();
		
		String header = "Planning voor week 31" + "\n";
		header += divider;
		
		String footer = divider + "\n";
		footer += "Exported by De Hartige Hap";
		
		return header + output + footer;
	}
}
