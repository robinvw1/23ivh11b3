package edu.avans.hartigehap.web.controller.notification;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.context.request.async.DeferredResult;

/**
 * NotificationResult, wrapper for DeferredResult
 * Delays the output and waits for an event from the Obvervable NotifcationService
 * @author derkgommers
 */
@Slf4j
public class NotificationResult implements Observer<Notification> {

	private String path;
	private Observable<Notification> observable;
	
	@Getter
	private DeferredResult<Notification> deferredResult;
	
	/**
	 * Receives notifcations from Observable
	 * Waits until a notification does match
	 */
	@Override
	public void notify (Notification n) {
		if (path.matches(n.getRegex())) {
			this.deferredResult.setResult(n);
			this.observable.removeObserver(this);
			log.info("Matches "+this.path);
		} else {
			log.debug("NotificationResult: Mismatches "+this.path);
		}
	}	
	
	/**
	 * Create a new DeferredResult, with timeout handler
	 */
	private void createDeferredResult () {
		this.deferredResult = new DeferredResult<>();

		final Observer<Notification> self = this;
		this.deferredResult.onTimeout(new Runnable () {
			public void run() {
				observable.removeObserver(self);
				log.debug("NotificationResult: Expired request for "+path);
			}
		});
	}
	
	/**
	 * Constructs a new Notification'Waiter' for given path
	 * @param path
	 */
	public NotificationResult (String path) {
		super();
		this.path = path;
		this.observable = NotificationService.getInstance();
		this.observable.addObserver(this);
		this.createDeferredResult();
		
		log.debug("NotificationResult: Created, observing for "+this.path);
	}

	
}