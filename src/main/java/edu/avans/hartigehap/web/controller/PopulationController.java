package edu.avans.hartigehap.web.controller;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import edu.avans.hartigehap.service.RestaurantPopulatorService;

@Controller
public class PopulationController {
	
	@Autowired
	private RestaurantPopulatorService populatorService;

	@PostConstruct
	public void populate () {
		populatorService.createRestaurantsWithInventory();
	}
}
